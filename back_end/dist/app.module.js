"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const app_controller_1 = require("./app.controller");
const auth_module_1 = require("./auth/auth.module");
const logger_module_1 = require("./logger/logger.module");
const user_module_1 = require("./user/user.module");
const mongoose_1 = require("@nestjs/mongoose");
const nestjs_1 = require("@automapper/nestjs");
const classes_1 = require("@automapper/classes");
const mail_module_1 = require("./mail/mail.module");
const upload_module_1 = require("./upload/upload.module");
const event_emitter_1 = require("@nestjs/event-emitter");
const bull_1 = require("@nestjs/bull");
const manager_user_module_1 = require("./manager-user/manager-user.module");
let AppModule = class AppModule {
};
AppModule = __decorate([
    (0, common_1.Module)({
        imports: [
            config_1.ConfigModule.forRoot({
                isGlobal: true,
            }),
            bull_1.BullModule.forRoot({
                redis: {
                    host: 'redis',
                    port: 6379,
                },
            }),
            mongoose_1.MongooseModule.forRootAsync({
                imports: [config_1.ConfigModule],
                useFactory: async (configService) => ({
                    uri: configService.get('DB_URL') || 'mongodb://localhost:27017',
                    useNewUrlParser: true,
                    useUnifiedTopology: true,
                    useFindAndModify: false,
                }),
                inject: [config_1.ConfigService],
            }),
            nestjs_1.AutomapperModule.forRoot({
                options: [{ name: 'classMapper', pluginInitializer: classes_1.classes }],
                singular: true,
            }),
            event_emitter_1.EventEmitterModule.forRoot(),
            mail_module_1.MailModule,
            logger_module_1.LoggerModule,
            auth_module_1.AuthModule,
            user_module_1.UserModule,
            upload_module_1.UploadModule,
            manager_user_module_1.ManagerUserModule,
        ],
        controllers: [app_controller_1.AppController],
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map