/// <reference types="mongoose" />
import { AuthService } from './auth.service';
import { UserService } from 'src/user/user.service';
import { SignUpUserDto } from 'src/user/dto/signup-user.dto';
import { UserDto } from 'src/decorators/user.decorator';
import { SignInDto } from './dto/sign-in.dto';
import { MailService } from 'src/mail/mail.service';
export declare class AuthController {
    private authService;
    private userService;
    private mailService;
    constructor(authService: AuthService, userService: UserService, mailService: MailService);
    loginLocal(signInDto: SignInDto, req: any, res: any): Promise<void>;
    logOut(res: any, userDto: UserDto): Promise<void>;
    signup(signUpUserDto: SignUpUserDto): Promise<import("mongoose").LeanDocument<import("../user/schema/user.schema").UserDocument>>;
    refreshToken(req: any): Promise<any>;
    loginGithub(): void;
    githubAuthRedirect(req: any, res: any): Promise<void>;
}
