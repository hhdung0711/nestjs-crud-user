"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthController = void 0;
const common_1 = require("@nestjs/common");
const passport_1 = require("@nestjs/passport");
const auth_service_1 = require("./auth.service");
const local_auth_guard_1 = require("./guards/local-auth.guard");
const swagger_1 = require("@nestjs/swagger");
const user_service_1 = require("../user/user.service");
const signup_user_dto_1 = require("../user/dto/signup-user.dto");
const jwt_resfresh_auth_guard_1 = __importDefault(require("./guards/jwt-resfresh-auth.guard"));
const jwt_access_auth_guard_1 = __importDefault(require("./guards/jwt-access-auth.guard"));
const user_decorator_1 = require("../decorators/user.decorator");
const sign_in_dto_1 = require("./dto/sign-in.dto");
const mail_service_1 = require("../mail/mail.service");
const view_auth_filter_1 = require("./filter/view-auth.filter");
let AuthController = class AuthController {
    constructor(authService, userService, mailService) {
        this.authService = authService;
        this.userService = userService;
        this.mailService = mailService;
    }
    async loginLocal(signInDto, req, res) {
        const user = req.user._doc;
        const accessTokenCookie = this.authService.getCookieWithJwtAccessToken(user._id);
        const { refreshToken, refreshCookie } = this.authService.getCookieWithJwtRefreshToken(user._id);
        await this.userService.setCurrentRefreshToken(refreshToken, user._id);
        console.log('accessTokenCookie, cookie', accessTokenCookie, refreshCookie);
        res.setHeader('Set-Cookie', [accessTokenCookie, refreshCookie]);
        res.redirect('/');
    }
    async logOut(res, userDto) {
        await this.userService.removeRefreshToken(userDto.userId);
        res.setHeader('Set-Cookie', this.authService.getCookiesForLogOut());
        res.redirect('/');
    }
    async signup(signUpUserDto) {
        const result = await this.userService.create(signUpUserDto);
        this.mailService.verifyEmailAddress(result.email, result.tokenActiveEmail);
        return result;
    }
    async refreshToken(req) {
        const user = req.user;
        const accessTokenCookie = this.authService.getCookieWithJwtAccessToken(user._id);
        req.res.setHeader('Set-Cookie', accessTokenCookie);
        return user;
    }
    loginGithub() { }
    async githubAuthRedirect(req, res) {
        const user = req.user;
        const accessTokenCookie = this.authService.getCookieWithJwtAccessToken(user._id);
        const { refreshToken, refreshCookie } = this.authService.getCookieWithJwtRefreshToken(user._id);
        await this.userService.setCurrentRefreshToken(refreshToken, user._id);
        console.log('accessTokenCookie, cookie', accessTokenCookie, refreshCookie);
        res.setHeader('Set-Cookie', [accessTokenCookie, refreshCookie]);
        res.redirect('/');
    }
};
__decorate([
    (0, common_1.UseGuards)(local_auth_guard_1.LocalAuthGuard),
    (0, common_1.Post)('sign_in'),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Request)()),
    __param(2, (0, common_1.Response)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [sign_in_dto_1.SignInDto, Object, Object]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "loginLocal", null);
__decorate([
    (0, common_1.UseGuards)(jwt_access_auth_guard_1.default),
    (0, common_1.Post)('log_out'),
    __param(0, (0, common_1.Response)()),
    __param(1, (0, user_decorator_1.User)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, user_decorator_1.UserDto]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "logOut", null);
__decorate([
    (0, common_1.Post)('sign_up'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [signup_user_dto_1.SignUpUserDto]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "signup", null);
__decorate([
    (0, common_1.UseGuards)(jwt_resfresh_auth_guard_1.default),
    (0, common_1.Post)('refresh_token'),
    __param(0, (0, common_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "refreshToken", null);
__decorate([
    (0, common_1.Get)('login/github'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('github')),
    (0, common_1.UseFilters)(view_auth_filter_1.ViewAuthFilter),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], AuthController.prototype, "loginGithub", null);
__decorate([
    (0, common_1.Get)('login/github/redirect'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('github')),
    (0, common_1.UseFilters)(view_auth_filter_1.ViewAuthFilter),
    __param(0, (0, common_1.Request)()),
    __param(1, (0, common_1.Response)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "githubAuthRedirect", null);
AuthController = __decorate([
    (0, swagger_1.ApiTags)('auth'),
    (0, common_1.Controller)('auth'),
    __metadata("design:paramtypes", [auth_service_1.AuthService,
        user_service_1.UserService,
        mail_service_1.MailService])
], AuthController);
exports.AuthController = AuthController;
//# sourceMappingURL=auth.controller.js.map