"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthModule = void 0;
const common_1 = require("@nestjs/common");
const auth_service_1 = require("./auth.service");
const local_strategy_1 = require("./strategies/local.strategy");
const jwt_access_token_strategy_1 = require("./strategies/jwt-access-token.strategy");
const user_module_1 = require("../user/user.module");
const passport_1 = require("@nestjs/passport");
const jwt_1 = require("@nestjs/jwt");
const constants_1 = require("./constants");
const google_strategy_1 = require("./strategies/google.strategy");
const config_1 = require("@nestjs/config");
const auth_controller_1 = require("./auth.controller");
const github_strategy_1 = require("./strategies/github.strategy");
const facebook_strategy_1 = require("./strategies/facebook.strategy");
const linkedin_strategy_1 = require("./strategies/linkedin.strategy");
const jwt_refresh_token_strategy_1 = require("./strategies/jwt-refresh-token.strategy");
const mail_module_1 = require("../mail/mail.module");
const admin_strategy_1 = require("./strategies/admin.strategy");
let AuthModule = class AuthModule {
};
AuthModule = __decorate([
    (0, common_1.Module)({
        imports: [
            user_module_1.UserModule,
            passport_1.PassportModule,
            jwt_1.JwtModule.register({
                secret: constants_1.jwtConstants.secret,
                signOptions: { expiresIn: '60s' },
            }),
            config_1.ConfigModule,
            mail_module_1.MailModule,
        ],
        providers: [
            auth_service_1.AuthService,
            local_strategy_1.LocalStrategy,
            jwt_access_token_strategy_1.JwtAccessTokenStrategy,
            jwt_refresh_token_strategy_1.JwtRefreshTokenStrategy,
            google_strategy_1.GoogleStrategy,
            github_strategy_1.GithubStrategy,
            facebook_strategy_1.FacebookStrategy,
            linkedin_strategy_1.LinkedinStrategy,
            admin_strategy_1.AdminStrategy,
        ],
        controllers: [auth_controller_1.AuthController],
        exports: [auth_service_1.AuthService],
    })
], AuthModule);
exports.AuthModule = AuthModule;
//# sourceMappingURL=auth.module.js.map