import { UserService } from '../user/user.service';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
export declare class AuthService {
    private userService;
    private jwtService;
    private configService;
    constructor(userService: UserService, jwtService: JwtService, configService: ConfigService);
    validateUser(email: string, password: string): Promise<any>;
    validateUserAdmin(email: string, password: string): Promise<any>;
    getCookieWithJwtAccessToken(userId: string): string;
    getCookieWithJwtRefreshToken(userId: string): {
        refreshCookie: string;
        refreshToken: string;
    };
    getCookiesForLogOut(): string[];
}
