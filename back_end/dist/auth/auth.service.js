"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthService = void 0;
const common_1 = require("@nestjs/common");
const user_service_1 = require("../user/user.service");
const jwt_1 = require("@nestjs/jwt");
const config_1 = require("@nestjs/config");
const status_enum_1 = require("../user/enum/status.enum");
let AuthService = class AuthService {
    constructor(userService, jwtService, configService) {
        this.userService = userService;
        this.jwtService = jwtService;
        this.configService = configService;
    }
    async validateUser(email, password) {
        const user = await this.userService.findByEmail(email);
        if (user && await this.userService.compareHash(password, user.password)) {
            if (user.status !== status_enum_1.UserStatus.Active) {
                throw new common_1.HttpException('Oops! It looks like you haven’t confirmed your email yet!', common_1.HttpStatus.UNAUTHORIZED);
            }
            const { password } = user, result = __rest(user, ["password"]);
            return result;
        }
        return null;
    }
    async validateUserAdmin(email, password) {
        const user = await this.userService.findByEmail(email);
        if (user && await this.userService.compareHash(password, user.password) && user.role === 'admin') {
            if (user.status !== status_enum_1.UserStatus.Active) {
                throw new common_1.HttpException('Oops! It looks like you haven’t confirmed your email yet!', common_1.HttpStatus.UNAUTHORIZED);
            }
            const { password } = user, result = __rest(user, ["password"]);
            return result;
        }
        return null;
    }
    getCookieWithJwtAccessToken(userId) {
        const payload = { userId };
        console.log(`${this.configService.get('JWT_ACCESS_TOKEN_EXPIRATION_TIME')}`);
        const token = this.jwtService.sign(payload, {
            secret: this.configService.get('JWT_ACCESS_TOKEN_SECRET'),
            expiresIn: `${this.configService.get('JWT_ACCESS_TOKEN_EXPIRATION_TIME')}`
        });
        return `Authentication=${token}; HttpOnly; Path=/;`;
    }
    getCookieWithJwtRefreshToken(userId) {
        const payload = { userId };
        const refreshToken = this.jwtService.sign(payload, {
            secret: this.configService.get('JWT_REFRESH_TOKEN_SECRET'),
            expiresIn: `${this.configService.get('JWT_REFRESH_TOKEN_EXPIRATION_TIME')}`
        });
        const refreshCookie = `Refresh=${refreshToken}; HttpOnly; Path=/; `;
        return {
            refreshCookie,
            refreshToken
        };
    }
    getCookiesForLogOut() {
        return [
            'Authentication=; HttpOnly; Path=/; Max-Age=0',
            'Refresh=; HttpOnly; Path=/; Max-Age=0'
        ];
    }
};
AuthService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [user_service_1.UserService,
        jwt_1.JwtService,
        config_1.ConfigService])
], AuthService);
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map