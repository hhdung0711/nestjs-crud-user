export declare const ALLOW_ANONYMOUS_META_KEY = "allowAnonymous";
export declare const AllowAnonymous: () => import("@nestjs/common").CustomDecorator<string>;
