"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AllowAnonymous = exports.ALLOW_ANONYMOUS_META_KEY = void 0;
const common_1 = require("@nestjs/common");
exports.ALLOW_ANONYMOUS_META_KEY = 'allowAnonymous';
const AllowAnonymous = () => (0, common_1.SetMetadata)(exports.ALLOW_ANONYMOUS_META_KEY, true);
exports.AllowAnonymous = AllowAnonymous;
//# sourceMappingURL=allow-anonymous.decorator.js.map