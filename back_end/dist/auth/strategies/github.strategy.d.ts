import { VerifyCallback } from 'passport-github2';
import { ConfigService } from '@nestjs/config';
import { UserService } from 'src/user/user.service';
declare const GithubStrategy_base: new (...args: any[]) => any;
export declare class GithubStrategy extends GithubStrategy_base {
    private config;
    private userService;
    constructor(config: ConfigService, userService: UserService);
    validate(accessToken: string, refreshToken: string, profile: any, done: VerifyCallback): Promise<any>;
}
export {};
