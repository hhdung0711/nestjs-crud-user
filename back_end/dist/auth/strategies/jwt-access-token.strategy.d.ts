import { Strategy } from 'passport-jwt';
import { ConfigService } from '@nestjs/config';
declare const JwtAccessTokenStrategy_base: new (...args: any[]) => Strategy;
export declare class JwtAccessTokenStrategy extends JwtAccessTokenStrategy_base {
    private readonly configService;
    constructor(configService: ConfigService);
    validate(payload: any): Promise<{
        userId: any;
    }>;
}
export {};
