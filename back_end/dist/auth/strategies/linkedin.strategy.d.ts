import { VerifyCallback } from 'passport-linkedin-oauth2';
import { ConfigService } from '@nestjs/config';
declare const LinkedinStrategy_base: new (...args: any[]) => any;
export declare class LinkedinStrategy extends LinkedinStrategy_base {
    constructor(config: ConfigService);
    validate(accessToken: string, refreshToken: string, profile: any, done: VerifyCallback): Promise<any>;
}
export {};
