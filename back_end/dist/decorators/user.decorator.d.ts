export declare const User: (...dataOrPipes: unknown[]) => ParameterDecorator;
export declare class UserDto {
    readonly userId: string;
}
