import { ArgumentsHost, ExceptionFilter, HttpException } from '@nestjs/common';
import { LoggerService } from '../logger/logger.service';
export declare class AllExceptionFilter implements ExceptionFilter {
    private logger;
    constructor(logger: LoggerService);
    private static handleResponse;
    catch(exception: HttpException | Error, host: ArgumentsHost): void;
    private handleMessage;
}
