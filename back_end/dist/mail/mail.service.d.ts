import { MailerService } from '@nestjs-modules/mailer';
export declare class MailService {
    private mailerService;
    constructor(mailerService: MailerService);
    verifyEmailAddress(to: string, token: string): Promise<void>;
}
