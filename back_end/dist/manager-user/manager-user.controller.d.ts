import { ManagerUserService } from './manager-user.service';
import { CreateUserDto } from './create-user.dto';
export declare class ManagerUserController {
    private userService;
    constructor(userService: ManagerUserService);
    getUsers(): Promise<any>;
    getUser(userId: any): Promise<any>;
    addUser(createUserDto: CreateUserDto): Promise<any>;
    deleteUser(query: any): Promise<any>;
}
