"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ManagerUserController = void 0;
const common_1 = require("@nestjs/common");
const manager_user_service_1 = require("./manager-user.service");
const create_user_dto_1 = require("./create-user.dto");
let ManagerUserController = class ManagerUserController {
    constructor(userService) {
        this.userService = userService;
    }
    async getUsers() {
        const users = await this.userService.getUsers();
        return users;
    }
    async getUser(userId) {
        const user = await this.userService.getUser(userId);
        return user;
    }
    async addUser(createUserDto) {
        const user = await this.userService.addUser(createUserDto);
        return user;
    }
    async deleteUser(query) {
        const users = await this.userService.deleteUser(query.userId);
        return users;
    }
};
__decorate([
    (0, common_1.Get)(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], ManagerUserController.prototype, "getUsers", null);
__decorate([
    (0, common_1.Get)(':userId'),
    __param(0, (0, common_1.Param)('userId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ManagerUserController.prototype, "getUser", null);
__decorate([
    (0, common_1.Post)(),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_user_dto_1.CreateUserDto]),
    __metadata("design:returntype", Promise)
], ManagerUserController.prototype, "addUser", null);
__decorate([
    (0, common_1.Delete)(),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ManagerUserController.prototype, "deleteUser", null);
ManagerUserController = __decorate([
    (0, common_1.Controller)('manager-user'),
    __metadata("design:paramtypes", [manager_user_service_1.ManagerUserService])
], ManagerUserController);
exports.ManagerUserController = ManagerUserController;
//# sourceMappingURL=manager-user.controller.js.map