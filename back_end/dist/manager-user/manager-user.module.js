"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ManagerUserModule = void 0;
const common_1 = require("@nestjs/common");
const manager_user_controller_1 = require("./manager-user.controller");
const manager_user_service_1 = require("./manager-user.service");
let ManagerUserModule = class ManagerUserModule {
};
ManagerUserModule = __decorate([
    (0, common_1.Module)({
        controllers: [manager_user_controller_1.ManagerUserController],
        providers: [manager_user_service_1.ManagerUserService]
    })
], ManagerUserModule);
exports.ManagerUserModule = ManagerUserModule;
//# sourceMappingURL=manager-user.module.js.map