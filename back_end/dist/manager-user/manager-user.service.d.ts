export declare class ManagerUserService {
    users: {
        id: number;
        name: string;
        password: string;
    }[];
    getUsers(): Promise<any>;
    getUser(userId: any): Promise<any>;
    addUser(user: any): Promise<any>;
    deleteUser(userId: any): Promise<any>;
}
