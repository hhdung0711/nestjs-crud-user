"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ManagerUserService = void 0;
const common_1 = require("@nestjs/common");
const manager_user_mock_1 = require("./manager-user.mock");
let ManagerUserService = class ManagerUserService {
    constructor() {
        this.users = manager_user_mock_1.USERS;
    }
    getUsers() {
        return new Promise(resolve => {
            resolve(this.users);
        });
    }
    getUser(userId) {
        let id = Number(userId);
        return new Promise(resolve => {
            const user = this.users.find(user => user.id === id);
            if (!user) {
                throw new common_1.HttpException('User not exits', 404);
            }
            resolve(user);
        });
    }
    addUser(user) {
        return new Promise(resolve => {
            this.users.push(user);
            resolve(this.users);
        });
    }
    deleteUser(userId) {
        let id = Number(userId);
        return new Promise(resolve => {
            let index = this.users.findIndex(user => user.id === id);
            if (index === -1) {
                throw new common_1.HttpException('user not exist', 404);
            }
            this.users.splice(index, 1);
            resolve(this.users);
        });
    }
};
ManagerUserService = __decorate([
    (0, common_1.Injectable)()
], ManagerUserService);
exports.ManagerUserService = ManagerUserService;
//# sourceMappingURL=manager-user.service.js.map