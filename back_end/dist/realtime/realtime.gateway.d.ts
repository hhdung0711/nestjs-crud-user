import { Server, Socket } from 'socket.io';
export declare class RealtimeGateway {
    server: Server;
    handleConnection(socket: Socket): Promise<void>;
    listenForMessages(data: string): void;
}
