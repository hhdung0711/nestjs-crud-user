import { UploadService } from './upload.service';
export declare class UploadController {
    private readonly uploadService;
    constructor(uploadService: UploadService);
    uploadedImage(file: any): Promise<{
        originalname: any;
        filename: any;
    }>;
    uploadMultipleImages(files: any): Promise<any[]>;
    uploadedFile(file: any): Promise<{
        originalname: any;
        filename: any;
    }>;
    seeUploadedFile(image: any, res: any): any;
}
