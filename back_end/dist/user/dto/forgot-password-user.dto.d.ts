export declare class ForgotPasswordUserDto {
    email: string;
    forgotPasswordCode: string;
    newPassword: string;
}
