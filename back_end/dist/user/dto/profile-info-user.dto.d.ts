export declare class ProfileInfoUserDto {
    readonly firstName: string;
    readonly lastName: string;
    readonly bio: string;
    readonly avatar_link: string;
    readonly github: string;
    readonly linkedin: string;
    readonly twitter: string;
    readonly website: string;
}
