export declare class SearchUserDto {
    readonly page: number;
    readonly pageSize: number;
    readonly search: string;
}
