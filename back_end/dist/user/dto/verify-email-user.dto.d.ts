export declare class VerifyEmailUserDto {
    email: string;
    token: string;
}
