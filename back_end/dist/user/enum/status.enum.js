"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserStatus = void 0;
var UserStatus;
(function (UserStatus) {
    UserStatus["Active"] = "Active";
    UserStatus["InActive"] = "InActive";
})(UserStatus = exports.UserStatus || (exports.UserStatus = {}));
//# sourceMappingURL=status.enum.js.map