import { Document, Schema as MongooseSchema } from 'mongoose';
export declare type UserDocument = User & Document;
export declare class User {
    userName: string;
    email: string;
    firstName: string;
    lastName: string;
    password: string;
    bio: string;
    avatar_link: string;
    github: string;
    linkedin: string;
    twitter: string;
    website: string;
    privateAccount: boolean;
    hideProfile: boolean;
    forgotPasswordCode: string;
    currentHashedRefreshToken: string;
    tokenActiveEmail: string;
    isConnectGithub: boolean;
    status: string;
    isFirstLogin: boolean;
}
export declare const UserSchema: MongooseSchema<Document<User, any, any>, import("mongoose").Model<Document<User, any, any>, any, any>, undefined, {}>;
