"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserSchema = exports.User = void 0;
const classes_1 = require("@automapper/classes");
const mongoose_1 = require("@nestjs/mongoose");
const status_enum_1 = require("../enum/status.enum");
let User = class User {
};
__decorate([
    (0, mongoose_1.Prop)(),
    (0, classes_1.AutoMap)(),
    __metadata("design:type", String)
], User.prototype, "userName", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    (0, classes_1.AutoMap)(),
    __metadata("design:type", String)
], User.prototype, "email", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    (0, classes_1.AutoMap)(),
    __metadata("design:type", String)
], User.prototype, "firstName", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    (0, classes_1.AutoMap)(),
    __metadata("design:type", String)
], User.prototype, "lastName", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    (0, classes_1.AutoMap)(),
    __metadata("design:type", String)
], User.prototype, "password", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    (0, classes_1.AutoMap)(),
    __metadata("design:type", String)
], User.prototype, "bio", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    (0, classes_1.AutoMap)(),
    __metadata("design:type", String)
], User.prototype, "avatar_link", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    (0, classes_1.AutoMap)(),
    __metadata("design:type", String)
], User.prototype, "github", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    (0, classes_1.AutoMap)(),
    __metadata("design:type", String)
], User.prototype, "linkedin", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    (0, classes_1.AutoMap)(),
    __metadata("design:type", String)
], User.prototype, "twitter", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    (0, classes_1.AutoMap)(),
    __metadata("design:type", String)
], User.prototype, "website", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    (0, classes_1.AutoMap)(),
    __metadata("design:type", Boolean)
], User.prototype, "privateAccount", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    (0, classes_1.AutoMap)(),
    __metadata("design:type", Boolean)
], User.prototype, "hideProfile", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", String)
], User.prototype, "forgotPasswordCode", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", String)
], User.prototype, "currentHashedRefreshToken", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", String)
], User.prototype, "tokenActiveEmail", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    (0, classes_1.AutoMap)(),
    __metadata("design:type", Boolean)
], User.prototype, "isConnectGithub", void 0);
__decorate([
    (0, mongoose_1.Prop)({
        enum: [status_enum_1.UserStatus.Active, status_enum_1.UserStatus.InActive],
        default: status_enum_1.UserStatus.InActive,
    }),
    (0, classes_1.AutoMap)(),
    __metadata("design:type", String)
], User.prototype, "status", void 0);
__decorate([
    (0, mongoose_1.Prop)({
        default: true,
    }),
    (0, classes_1.AutoMap)(),
    __metadata("design:type", Boolean)
], User.prototype, "isFirstLogin", void 0);
User = __decorate([
    (0, mongoose_1.Schema)()
], User);
exports.User = User;
exports.UserSchema = mongoose_1.SchemaFactory.createForClass(User);
//# sourceMappingURL=user.schema.js.map