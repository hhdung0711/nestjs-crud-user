/// <reference types="mongoose" />
import { UserService } from './user.service';
import { UserDto } from 'src/decorators/user.decorator';
import { SignUpUserDto } from './dto/signup-user.dto';
export declare class UserController {
    private readonly userService;
    constructor(userService: UserService);
    getMe(user: UserDto): Promise<import("./schema/user.schema").UserDocument>;
    getAllUser(): Promise<import("./schema/user.schema").UserDocument[]>;
    addUser(user: SignUpUserDto): Promise<import("mongoose").LeanDocument<import("./schema/user.schema").UserDocument>>;
    updateInfoUser(user: UserDto, signUpUserDto: SignUpUserDto): Promise<import("./schema/user.schema").UserDocument>;
    updateInfoGithub(user: UserDto, githubUserDto: any): Promise<import("./schema/user.schema").UserDocument>;
    deleteUserById(userId: string): Promise<import("./schema/user.schema").UserDocument>;
}
