"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const user_service_1 = require("./user.service");
const jwt_access_auth_guard_1 = __importDefault(require("../auth/guards/jwt-access-auth.guard"));
const user_decorator_1 = require("../decorators/user.decorator");
const signup_user_dto_1 = require("./dto/signup-user.dto");
let UserController = class UserController {
    constructor(userService) {
        this.userService = userService;
    }
    async getMe(user) {
        return this.userService.findById(user.userId);
    }
    async getAllUser() {
        return this.userService.findAll();
    }
    async addUser(user) {
        return this.userService.create(user);
    }
    async updateInfoUser(user, signUpUserDto) {
        return this.userService.updateInfoUser(user.userId, signUpUserDto);
    }
    async updateInfoGithub(user, githubUserDto) {
        const currentUser = await this.userService.findById(user.userId);
        if (!currentUser.isConnectGithub) {
            this.userService.updateConnectGithub(user.userId, true);
        }
        return this.userService.updateGithub(githubUserDto);
    }
    async deleteUserById(userId) {
        return this.userService.deleteById(userId);
    }
};
__decorate([
    (0, common_1.Get)('/me'),
    (0, common_1.UseGuards)(jwt_access_auth_guard_1.default),
    __param(0, (0, user_decorator_1.User)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_decorator_1.UserDto]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "getMe", null);
__decorate([
    (0, common_1.Get)('/userActive'),
    (0, common_1.UseGuards)(jwt_access_auth_guard_1.default),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], UserController.prototype, "getAllUser", null);
__decorate([
    (0, common_1.Post)(),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [signup_user_dto_1.SignUpUserDto]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "addUser", null);
__decorate([
    (0, common_1.Put)('/updateInfo'),
    (0, common_1.UseGuards)(jwt_access_auth_guard_1.default),
    __param(0, (0, user_decorator_1.User)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_decorator_1.UserDto,
        signup_user_dto_1.SignUpUserDto]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "updateInfoUser", null);
__decorate([
    (0, common_1.Put)('/updateInfoGithub'),
    (0, common_1.UseGuards)(jwt_access_auth_guard_1.default),
    __param(0, (0, user_decorator_1.User)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_decorator_1.UserDto, Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "updateInfoGithub", null);
__decorate([
    (0, common_1.Delete)('/:userId'),
    (0, common_1.UseGuards)(jwt_access_auth_guard_1.default),
    __param(0, (0, common_1.Param)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "deleteUserById", null);
UserController = __decorate([
    (0, swagger_1.ApiTags)('user'),
    (0, common_1.Controller)('user'),
    __metadata("design:paramtypes", [user_service_1.UserService])
], UserController);
exports.UserController = UserController;
//# sourceMappingURL=user.controller.js.map