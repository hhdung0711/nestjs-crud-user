"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserModule = void 0;
const common_1 = require("@nestjs/common");
const user_service_1 = require("./user.service");
const mongoose_1 = require("@nestjs/mongoose");
const user_schema_1 = require("./schema/user.schema");
const user_controller_1 = require("./user.controller");
const signup_user_dto_1 = require("./dto/signup-user.dto");
const mail_module_1 = require("../mail/mail.module");
;
let UserModule = class UserModule {
};
UserModule = __decorate([
    (0, common_1.Module)({
        imports: [
            mongoose_1.MongooseModule.forFeature([
                { name: 'user', schema: user_schema_1.UserSchema },
            ]),
            mail_module_1.MailModule,
        ],
        controllers: [user_controller_1.UserController],
        providers: [user_service_1.UserService, signup_user_dto_1.SignUpUserDto],
        exports: [user_service_1.UserService, signup_user_dto_1.SignUpUserDto],
    })
], UserModule);
exports.UserModule = UserModule;
//# sourceMappingURL=user.module.js.map