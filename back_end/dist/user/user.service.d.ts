import { SignUpUserDto } from './dto/signup-user.dto';
import { UserDocument } from './schema/user.schema';
import { Model } from 'mongoose';
export declare class UserService {
    private readonly UserModel;
    constructor(UserModel: Model<UserDocument>);
    setCurrentRefreshToken(refreshToken: string, userId: string): Promise<void>;
    removeRefreshToken(userId: string): Promise<void>;
    getUserIfRefreshTokenMatches(refreshToken: string, userId: string): Promise<UserDocument>;
    create(signUpUserDto: SignUpUserDto): Promise<import("mongoose").LeanDocument<UserDocument>>;
    getUserName(name: string): any;
    randomString(length: any): string;
    updateGithub(githubUserDto: any): Promise<UserDocument>;
    _getWebsite(link: string): string;
    updateConnectGithub(userId: string, isConnectGithub: boolean): Promise<void>;
    findById(userId: string): Promise<UserDocument>;
    findByUserName(userName: string): Promise<UserDocument>;
    deleteById(userId: string): Promise<UserDocument>;
    findByEmail(email: string): Promise<UserDocument>;
    findByIds(ids: string[], options?: {}): Promise<UserDocument[]>;
    updateStatusAndToken(email: any, status: string, token?: string): Promise<void>;
    updateVerifyEmailToken(email: any, token?: string): Promise<void>;
    findAll(): Promise<UserDocument[]>;
    hash(password: any): Promise<string>;
    compareHash(password: string, hash: string): Promise<boolean>;
    findOne(conditions: any): Promise<UserDocument>;
    updateInfoUser(userId: string, data: SignUpUserDto): Promise<UserDocument>;
}
