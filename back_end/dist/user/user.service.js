"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const bcrypt = __importStar(require("bcrypt"));
const status_enum_1 = require("./enum/status.enum");
const uuid_1 = require("uuid");
let UserService = class UserService {
    constructor(UserModel) {
        this.UserModel = UserModel;
    }
    async setCurrentRefreshToken(refreshToken, userId) {
        const currentHashedRefreshToken = await this.hash(refreshToken);
        await this.UserModel.findByIdAndUpdate(userId, { currentHashedRefreshToken: `${currentHashedRefreshToken}` });
    }
    async removeRefreshToken(userId) {
        await this.UserModel.findByIdAndUpdate(userId, { currentHashedRefreshToken: null });
    }
    async getUserIfRefreshTokenMatches(refreshToken, userId) {
        const user = await this.findById(userId);
        if (!user) {
            throw new common_1.HttpException('User with this id does not exist', common_1.HttpStatus.NOT_FOUND);
        }
        if (await this.compareHash(refreshToken, user.currentHashedRefreshToken)) {
            return user;
        }
    }
    async create(signUpUserDto) {
        const user = await this.findByEmail(signUpUserDto.email);
        if (user) {
            throw new common_1.HttpException('Email is exists!', common_1.HttpStatus.BAD_REQUEST);
        }
        const name = `${signUpUserDto.firstName}${signUpUserDto.lastName}`.split('.').join('');
        let userName = await this.getUserName(name);
        const createdUser = new this.UserModel(Object.assign(Object.assign({}, signUpUserDto), { userName, tokenActiveEmail: (0, uuid_1.v4)(8), password: await this.hash(signUpUserDto.password) }));
        const userFromDB = await createdUser.save();
        return userFromDB.toJSON();
    }
    async getUserName(name) {
        const user = await this.findOne({
            userName: name,
        });
        if (user) {
            const _name = `${name}${this.randomString(4)}`;
            return this.getUserName(_name);
        }
        return name;
    }
    randomString(length) {
        let result = '';
        let characters = '0123456789';
        let charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }
    async updateGithub(githubUserDto) {
        var _a;
        const userName = await this.getUserName(githubUserDto.login_name);
        const website = this._getWebsite(githubUserDto.website);
        return this.UserModel.findOneAndUpdate({
            email: githubUserDto.email,
        }, {
            firstName: githubUserDto.login_name,
            userName,
            bio: githubUserDto.bio,
            avatar_link: githubUserDto.avatar_link,
            github: (_a = githubUserDto.profileUrl) === null || _a === void 0 ? void 0 : _a.replace('https://github.com/', ''),
            isConnectGithub: true,
            twitter: githubUserDto.twitter,
            website,
        }, { upsert: true, new: true });
    }
    _getWebsite(link) {
        if (!link) {
            return '';
        }
        if (link.startsWith("http://") || link.startsWith("https://")) {
            return link;
        }
        return `http://${link}`;
    }
    async updateConnectGithub(userId, isConnectGithub) {
        await this.UserModel.findByIdAndUpdate(userId, { isConnectGithub });
    }
    async findById(userId) {
        return this.UserModel.findOne({
            _id: userId,
        }).exec();
    }
    async findByUserName(userName) {
        return this.UserModel.findOne({
            userName: userName,
        }).exec();
    }
    async deleteById(userId) {
        return await this.UserModel.findByIdAndDelete(userId);
    }
    async findByEmail(email) {
        return this.UserModel.findOne({ email: email }).exec();
    }
    async findByIds(ids, options = {}) {
        return this.UserModel.find({
            _id: {
                $in: ids,
            },
        }, options);
    }
    async updateStatusAndToken(email, status, token) {
        await this.UserModel.findOneAndUpdate({
            email: email,
        }, {
            status,
            tokenActiveEmail: token,
        });
    }
    async updateVerifyEmailToken(email, token) {
        await this.UserModel.findOneAndUpdate({
            email: email,
        }, { tokenActiveEmail: token });
    }
    async findAll() {
        return this.UserModel.find({
            status: status_enum_1.UserStatus.Active
        });
    }
    async hash(password) {
        const hash = await bcrypt.hash(password, 12);
        return hash;
    }
    async compareHash(password, hash) {
        const isMatch = await bcrypt.compare(password, hash);
        return isMatch;
    }
    async findOne(conditions) {
        return this.UserModel.findOne(conditions);
    }
    async updateInfoUser(userId, data) {
        return await this.UserModel.findByIdAndUpdate(userId, {
            firstName: data.firstName,
            lastName: data.lastName,
            email: data.email
        });
    }
};
UserService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, mongoose_1.InjectModel)('user')),
    __metadata("design:paramtypes", [mongoose_2.Model])
], UserService);
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map