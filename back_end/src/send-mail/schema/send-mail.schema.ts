import { AutoMap } from "@automapper/classes";
import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document, Schema as MongooseSchema } from "mongoose";
import { User } from "src/user/schema/user.schema";


export type SendMailDocument = SendMail & Document;

@Schema({ timestamps: { currentTime: () => new Date(Date.now()).getTime()  }})
export class SendMail {
    @Prop()
    @AutoMap()
    toUser: string;

    @Prop()
    @AutoMap()
    subject: string;

    @Prop()
    @AutoMap()
    content: string;

    @Prop()
    @AutoMap()
    sendDate: string;

    @Prop()
    @AutoMap()
    status: string;

    @AutoMap()
    del_flg: number;
    
    @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'user' })
    @AutoMap()
    createdBy: User;
    
    @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'user' })
    updatedBy: User;

}

export const SendMailSchema = SchemaFactory.createForClass(SendMail);