import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards } from '@nestjs/common';
import { SendMailService } from './send-mail.service';
import { CreateSendMailDto } from './dto/create-send-mail.dto';
import { UpdateSendMailDto } from './dto/update-send-mail.dto';
import JwtAccessAuthGuard from 'src/auth/guards/jwt-access-auth.guard';
import { User, UserDto } from 'src/decorators/user.decorator';
import { ApiTags } from '@nestjs/swagger';
import { MailService } from 'src/mail/mail.service';
import { InjectQueue } from '@nestjs/bull';
import { Queue } from 'bull';

@ApiTags('send-mail')
@Controller('send-mail')
export class SendMailController {
  constructor(
    private readonly sendMailService: SendMailService,
    private readonly mailService: MailService,
  ) {}

  @Post('/create')
  @UseGuards(JwtAccessAuthGuard)
  create(
    @User() user: UserDto,
    @Body() createSendMailDto: CreateSendMailDto) {
    return this.sendMailService.create(user.userId, createSendMailDto);
  }

  @Post('/sendmail/:id')
  @UseGuards(JwtAccessAuthGuard)
  async sendMail(
    @Param('id') id: string) {
    // this.mailService.sendMailTask(mail)
    this.sendMailService.sendMail(id);
    return true;
  }

  @Get()
  findAll() {
    return this.sendMailService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.sendMailService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateSendMailDto: UpdateSendMailDto) {
    return this.sendMailService.update(id, updateSendMailDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.sendMailService.remove(+id);
  }
}
