import { forwardRef, Module } from '@nestjs/common';
import { SendMailService } from './send-mail.service';
import { SendMailController } from './send-mail.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { SendMailSchema } from './schema/send-mail.schema';
import { UserModule } from 'src/user/user.module';
import { MailModule } from 'src/mail/mail.module';
import { BullModule } from '@nestjs/bull';
import { MailProcessor } from './sendMail.processor';

@Module({
  imports: [
    MongooseModule.forFeature([
      {name: 'sendmail', schema: SendMailSchema},
    ]),
    MailModule,
    forwardRef(() => UserModule),
    BullModule.registerQueueAsync({
      name: 'TaskSendMail',//config.get('mail.queue.name'),
      // useFactory: () => ({
      //   redis: {
      //     host: 'localhost',//config.get('mail.queue.host'),
      //     port: 5003, //config.get('mail.queue.port'),
      //   },
      // }),
    }),
  ],
  controllers: [SendMailController],
  providers: [SendMailService, MailProcessor]
})
export class SendMailModule {}
