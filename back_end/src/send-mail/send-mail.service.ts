import { InjectQueue } from '@nestjs/bull';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Queue } from 'bull';
import { Model } from 'mongoose';
import { UserService } from 'src/user/user.service';
import { CreateSendMailDto } from './dto/create-send-mail.dto';
import { UpdateSendMailDto } from './dto/update-send-mail.dto';
import { SendMailDocument } from './schema/send-mail.schema';

@Injectable()
export class SendMailService {

  constructor(
    private userService: UserService,
    @InjectModel('sendmail')
    private readonly SendMailModel: Model<SendMailDocument>,
    @InjectQueue('TaskSendMail')
    private mailQueue: Queue, 
  ) {}

  async sendMail(id: string) {
    const mail = await this.SendMailModel.findById(id);
    // this.mailService.sendMailTask(mail)
    await this.mailQueue.add('confirmation', mail, {delay: 5000});
  }

  async create(userId: string, createSendMailDto: CreateSendMailDto) {
    try {
      const user = await this.userService.findById(userId);

      const newMail = new this.SendMailModel({
          ...createSendMailDto,
          status: 'start',
          createdBy: user._id,
          updatedBy: user._id
      })

      const mailFromDB = await newMail.save();
      return mailFromDB.toJSON();
      
    } catch (error) {
      throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
    
  findAll() {
    try {
      return this.SendMailModel.find({});
    } catch (error) {
      throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  
  findOne(id: string) {
    return this.SendMailModel.findById(id);
  }
  
  async update(id: string, updateSendMailDto: UpdateSendMailDto) {
    try {
      const email = await this.SendMailModel.findOne({_id: id});
      if (!email || email.status === 'completed') {
        throw new HttpException("email khong hop le", HttpStatus.BAD_REQUEST);
      }
      
      return this.SendMailModel.findByIdAndUpdate(id, {subject: updateSendMailDto.subject, body: updateSendMailDto.body})

      // return ex
    } catch (error) {
      throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    return this.SendMailModel.findByIdAndUpdate(id,{status},{new: true})
    // return `This action updates a #${id} sendMail`;
  }
  
  async updateSendMailStatus(id: string, status: string) {
    try {
      return this.SendMailModel.findByIdAndUpdate(id,{status},{new: true})
    } catch (error) {
      
      throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  remove(id: number) {
    return `This action removes a #${id} sendMail`;
  }
}
