import { MailerService } from '@nestjs-modules/mailer'
import { OnQueueActive, OnQueueCompleted, OnQueueFailed, Process, Processor } from '@nestjs/bull'
import { Logger } from '@nestjs/common'
import { Job } from 'bull'
import { SendMailService } from './send-mail.service'

@Processor('TaskSendMail')
export class MailProcessor {
  constructor(
    private readonly mailerService: MailerService,
    private readonly sendMailService: SendMailService,

  ) {}

  @OnQueueActive()
  async onActive(job: Job) {
    const mail: any = job.data
    await this.sendMailService.updateSendMailStatus(mail._id,'pendding')
    console.log(`Processing job ${job.id} of type ${job.name}. Data: ${JSON.stringify(job.data)}`)
  }
  
  @OnQueueCompleted()
  async onComplete(job: Job, result: any) {
    const mail: any = job.data
    await this.sendMailService.updateSendMailStatus(mail._id, 'completed')
    console.log(`Completed job ${job.id} of type ${job.name}. Result: ${JSON.stringify(result)}`)
  }
  
  @OnQueueFailed()
  async onError(job: Job<any>, error: any) {
    const mail: any = job.data
    await this.sendMailService.updateSendMailStatus(mail._id, 'fail')
    console.log(`Failed job ${job.id} of type ${job.name}: ${error.message}`, error.stack)
  }

  // @Process('confirmation1')
  // async sendWelcomeEmail1(job:Job<unknown>){
  //   const mail: any = job.data
  //   console.log("===========testt: ")//,mail.toUser)
  // }
  //   async sendWelcomeEmail(job: Job<{ user: User, code: string }>): Promise<any> {
  @Process('confirmation')
  async sendWelcomeEmail(job:Job<unknown>){

    const mail: any = job.data

    const contentMail = mail.content;

    try {
      const result = await this.mailerService.sendMail({
        template: './task-email.hbs',
        context: { // ✏️ filling curly brackets with content
            contentMail,
        },
        from: '"Support Team" <support@example.com>', // override default from
        to: mail.toUser,
        subject: `Welcome to ${mail.subject}! Please Confirm Your Email Address`,
      })
      return result

    } catch (error) {
      // this.logger.error(`Failed to send confirmation email to '${job.data.mailDto.toUser}'`, error.stack)
      throw error
    }
  }
}
