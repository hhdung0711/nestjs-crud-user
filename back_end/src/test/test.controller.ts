import { Controller, Get, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TestService } from './test.service';

@ApiTags('test')
@Controller('test')
export class TestController {
    constructor(
        private readonly messageProducerService:TestService) {}
        
      @Get('invoke-msg')
      getInvokeMsg(@Query('msg') msg:string){
        this.messageProducerService.sendMessage(msg);
        return msg;
      }
}
