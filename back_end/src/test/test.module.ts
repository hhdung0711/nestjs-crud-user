import { BullModule } from '@nestjs/bull';
import { Module } from '@nestjs/common';
import { MessageConsumer } from './message.consumer';
import { TestController } from './test.controller';
import { TestService } from './test.service';

@Module({
    imports: [
        BullModule.registerQueue({
        name:'message-queue'
        })
    ],
    controllers: [TestController],
    providers: [TestService, MessageConsumer]
})
export class TestModule {}
